# Spin, an AWS Instance Launcher
I built this to learn about AWS and Golang.

[Download](http://bit.ly/djhaskin987-spin)
# To Build
I use [glide](https://github.com/Masterminds/glide) for dep management, and
this project is written in [golang](https://golang.org/). Once those tools
are installed, simply run
```bash
./buildme
```

# Before You start
Create a file called `~/.aws/credentials` and put something like this in it:
```
[default]
aws_access_key_id=ACCESSKEY
aws_secret_access_key=ACCESSKEYSECRET
```
Export the following environment variable:
```
export AWS_SDK_LOAD_CONFIG=1
```

# Go!

Just run
```
./spin help
```
To see how it works.
