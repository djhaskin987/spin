package main

import (
	"flag"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"log"
	"os"
)

func Describe() error {
	sess, err := session.NewSession()
	if err != nil {
		log.Println("Problem creating session")
		return err
	}

	svc := ec2.New(sess, &aws.Config{Region: aws.String("us-west-2")})
	// Call the DescribeInstances Operation
	resp, err := svc.DescribeInstances(nil)
	if err != nil {
		log.Println("Problem calling DesribeInstance")
		return err
	}

	// resp has all of the response data, pull out instance IDs:
	log.Println("> Number of reservation sets: ", len(resp.Reservations))
	for idx, res := range resp.Reservations {
		log.Println("  > Number of instances: ", len(res.Instances))
		for _, inst := range resp.Reservations[idx].Instances {
			log.Println("    - Instance ID: ", *inst.InstanceId)
		}
	}
	return nil
}

func SpinUp(id string) error {
	log.Printf("Spinning up `%s`...\n", id)
	svc := ec2.New(session.New(&aws.Config{Region: aws.String("us-west-2")}))
	// Specify the details of the instance that you want to create.
	runResult, runErr := svc.RunInstances(&ec2.RunInstancesInput{
		ImageId:      aws.String(id),
		InstanceType: aws.String("t2.micro"),
		KeyName:      aws.String("djhaskin987-gpg"),
		MaxCount:     aws.Int64(1),
		MinCount:     aws.Int64(1),
	})

	if runErr != nil {
		log.Println("Could not create instance", runErr)
		return runErr
	}

	log.Printf("Run result: %#v\n", runResult)
	return nil

}

func Terminate(id string) error {
	log.Printf("Terminating `%s`...\n", id)
	svc := ec2.New(session.New(&aws.Config{Region: aws.String("us-west-2")}))
	// Specify the details of the instance that you want to create.
	result, err := svc.TerminateInstances(&ec2.TerminateInstancesInput{
		InstanceIds: []*string{aws.String(id)},
	})

	if err != nil {
		log.Println("Could not create instance", err)
		return err
	}

	log.Printf("Termination result: %#v\n", result)
	return nil

}

func SpinDown(id string) error {
	log.Printf("Spinning down `%s`...\n", id)
	svc := ec2.New(session.New(&aws.Config{Region: aws.String("us-west-2")}))
	// Specify the details of the instance that you want to create.
	stopResult, stopErr := svc.StopInstances(&ec2.StopInstancesInput{
		InstanceIds: []*string{aws.String(id)},
	})

	if stopErr != nil {
		log.Println("Could not stop instance", stopErr)
		return stopErr
	}

	log.Printf("Stop result: %#v\n", stopResult)

	return nil
}

func Usage() {
	fmt.Println("Usage: spin <command> [<args>]")
	fmt.Println("Commands available are: ")
	fmt.Println(" help               Print this help page")
	fmt.Println(" up <image-id>              Spin up an AWS instance")
	fmt.Println(" describe                   List AWS instances")
	fmt.Println(" terminate <instance-id>    Terminate an AWS instances")
	fmt.Println(" down <instance-id>         Spin down an AWS instance")
	os.Exit(1)
}

func main() {
	if len(os.Args) == 1 {
		Usage()
	}

	upCommand := flag.NewFlagSet("up", flag.ExitOnError)
	downCommand := flag.NewFlagSet("down", flag.ExitOnError)
	describeCommand := flag.NewFlagSet("describe", flag.ExitOnError)
	terminateCommand := flag.NewFlagSet("terminate", flag.ExitOnError)
	switch os.Args[1] {
	case "help":
		Usage()
	case "up":
		upCommand.Parse(os.Args[2:])
	case "down":
		downCommand.Parse(os.Args[2:])
	case "describe":
		describeCommand.Parse(os.Args[2:])
	case "terminate":
		terminateCommand.Parse(os.Args[2:])
	default:
		log.Printf("Given argument `%q` is not a valid command.\n", os.Args[1])
	}

	if upCommand.Parsed() {
		if upCommand.NArg() < 1 {
			Usage()
		}
		amiID := upCommand.Arg(0)
		if err := SpinUp(amiID); err != nil {
			log.Fatalf("Problem with spinning up an instance from `%s`: `%s`\n",
				amiID, err)
		}
	} else if downCommand.Parsed() {
		if downCommand.NArg() < 1 {
			Usage()
		}
		instanceID := downCommand.Arg(0)
		if err := SpinDown(instanceID); err != nil {
			log.Fatalf("Problem with spinning down instance `%s`: `%s`\n",
				instanceID, err)
		}
	} else if terminateCommand.Parsed() {
		if terminateCommand.NArg() < 1 {
			Usage()
		}
		instanceID := terminateCommand.Arg(0)
		if err := Terminate(instanceID); err != nil {
			log.Fatalf("Problem with terminating instance: `%s`\n",
				instanceID, err)
		}
	} else if describeCommand.Parsed() {
		if err := Describe(); err != nil {
			log.Fatalf("Problem with describe: `%s`\n", err)
		}
	}
}
